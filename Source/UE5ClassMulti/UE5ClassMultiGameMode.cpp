// Copyright Epic Games, Inc. All Rights Reserved.

#include "UE5ClassMultiGameMode.h"
#include "UE5ClassMultiCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUE5ClassMultiGameMode::AUE5ClassMultiGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/BP_TPC2"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
