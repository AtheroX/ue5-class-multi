// Fill out your copyright notice in the Description page of Project Settings.


#include "RepCube.h"
#include "Net/UnrealNetwork.h"

#include "Kismet/KismetMathLibrary.h"

// Sets default values
ARepCube::ARepCube()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	NetCullDistanceSquared = 300000.f;
	mpCube = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cube"));
	RootComponent = mpCube;
	
}

void ARepCube::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {

	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ARepCube, mColor);
}

void ARepCube::OnRep_mColor() {
	if(mpMat)
		mpMat->SetVectorParameterValue(FName("Color"), mColor);
}

void ARepCube::ServerSetColor() {
	double r {UKismetMathLibrary::RandomFloat()};
	double g {UKismetMathLibrary::RandomFloat()};
	double b {UKismetMathLibrary::RandomFloat()};

	mColor = FLinearColor(r,g,b,1);
	OnRep_mColor();
}

// Called when the game starts or when spawned
void ARepCube::BeginPlay()
{
	Super::BeginPlay();

	mpMat = mpCube->CreateDynamicMaterialInstance(0, mpCube->GetMaterial(0));
	mpMat->SetVectorParameterValue(FName("Color"), FLinearColor::Blue);

	if(HasAuthority())
		GetWorld()->GetTimerManager().SetTimer(mTimer, this, &ARepCube::ServerSetColor, 1, true);
}

// Called every frame
void ARepCube::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

