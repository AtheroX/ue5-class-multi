// Copyright Epic Games, Inc. All Rights Reserved.

#include "UE5ClassMulti.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UE5ClassMulti, "UE5ClassMulti" );
 